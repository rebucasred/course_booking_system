//target the container from the html document
let adminControls = document.querySelector('#adminButton')
let container = document.querySelector('#coursesContainer')

let token = localStorage.getItem("token")

//capture the properties currently stored in web storage
const isAdmin = localStorage.getItem("isAdmin")

if (isAdmin == "false" || !isAdmin) {
	adminControls.innerHTML = null
/*	container.innerHTML = `
	<div class="col-md-6 my-3">
		<div class="card">
			<div class="card-body">
				<h3 class="card-title">Course Name</h3>
				<p class="card-text text-left">Description</p>
				<p class="card-text text-left">Price</p>
				<p class="card-text text-left">Created On</p>
			</div>
		</div>
	</div>
	`*/
} else {
	adminControls.innerHTML = `
		<div class="col-md-12">
			<a href="./addCourse.html" class="btn btn-block btn-warning">Add Course</a>
		</div>
	`
}

// send a request to retrieve all documents from courses collection
fetch('https://desolate-ocean-63477.herokuapp.com/api/courses').then(res => res.json()).then(jsonData => {
	
	//checker for output
	//console.log(jsonData)

	//display the result depending on the return
	let courseData

	//control structure for the variable
	if (jsonData.length < 1) {
		courseData = `<h2>No Courses Available</h2>`
		container.innerHTML = courseData
	} else {
		//display the content of the array
		courseData = jsonData.map(course => {
			console.log(course)
			console.log(course._id)

			//inject the properties of the object inside a card component
			
			let courseControls = ""

			if (isAdmin == "false" || !isAdmin) {
				courseControls = `<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>`
			} else {
				courseControls = `
							<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>	
							<a href="#" class="btn btn-primary text-white btn-block">Edit</a>
							<a href="#" class="btn btn-danger text-white btn-block">Delete</a>`
			}

			if(isAdmin===null){
				courseControls = `<a href="./pages/login.html" class="btn btn-success text-white btn-block">Login to view</a>`
			}

			return(`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-header bg-secondary"><h3 class="text-white">${course.name}</h3></div>
						<div class="card-body">
							<p class="card-text text-left" id="crdbdy">${course.description}</p>
						</div>
						<div class="card-footer bg-success">
							${courseControls}
						</div>
					</div>
				</div>
			`)

		}).join("") //create a return of new string
		container.innerHTML = courseData
	}
})

if (token===null) {
	window.location.replace('./login.html')
}