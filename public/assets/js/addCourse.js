// create add course page
let formSubmit = document.querySelector('#createCourse')
let token = localStorage.getItem("token")
const isAdmin = localStorage.getItem("isAdmin")

// acquire an event that will be applied in our form component
// create a subfunction inside the method to describe the procedure/action that will take place upon triggering the event
formSubmit.addEventListener("submit", (event) => {
	event.preventDefault() //this will avoid page redirection

	let name = document.querySelector("#courseName").value
	let cost = document.querySelector("#coursePrice").value
	let desc = document.querySelector("#courseDesc").value

	// checker to see if values are captured
	// console.log(name)
	// console.log(cost)
	// console.log(desc)

	// sedn a request to the backend project to process the data for creating a new entry inside our courses collection

	// upon creating this fetch api request, we are instantiating a promise
	if (name !== "" && cost !== "" && desc !== ""){
		fetch('https://desolate-ocean-63477.herokuapp.com/api/courses/course-exists',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name
			})
		}).then(res => {
			return res.json()
		}).then(data => {
			if (data === false) {
				fetch('https://desolate-ocean-63477.herokuapp.com/api/courses/create', {
				method: 'POST',
				headers: {
				'Content-Type': 'application/json'
					},
				body: JSON.stringify({
					name: name,
					description: desc,
					price: cost
					})
				}).then(res => {
					// console.log(res)
					return res.json()
				}).then(info => {
					// console.log(info)
					if (info === true) {
						Swal.fire({
							icon: 'success',
							text: 'Course Successfully Added!'
						})
					} else {
						Swal.fire({
							icon: 'info',
							text: 'Something went wrong during processing'
						})
					}
				})
			} else {
				Swal.fire({
					icon: 'error',
					text: 'Course already exists'
				})
			}
		})
	}else{
		Swal.fire({
			icon: 'error',
			text: 'All fields are required'
		})
	}
})

if (isAdmin == "false" || !isAdmin) {
	addcourseform.innerHTML = `<h2>Sorry, this page is not available for non-admin</h2><div><a href="./login.html" class="btn btn-outline-primary">Login Admin Access<a></div>`
	window.location.replace('./login.html')
}

if (token===null) {
	window.location.replace('./login.html')
}