let navItem = document.querySelector("#navSession")

let userToken = localStorage.getItem("token")
const checkAdmin = localStorage.getItem("isAdmin")

if (!userToken) {
	navItem.innerHTML = `
		<li class="nav-item"><a href="./login.html" class="nav-link">Login</a></li>
	`
} else {
	navItem.innerHTML = `
		<li class="nav-item"><a href="./courses.html" class="nav-link">Courses</a></li>
		<li class="nav-item"><a href="./profile.html" class="nav-link">Profile</a></li>
		<li class="nav-item"><a href="./logout.html" class="nav-link">Logout</a></li>
	`
}


if(checkAdmin === "true"){
	navItem.innerHTML = `
		<li class="nav-item"><a href="./index.html" class="nav-link">Home</a></li>
		<li class="nav-item"><a href="./logout.html" class="nav-link">Logout</a></li>
	`
}else{
	navItem.innerHTML = `
		<li class="nav-item"><a href="./index.html" class="nav-link">Home</a></li>
		<li class="nav-item"><a href="./courses.html" class="nav-link">Courses</a></li>
		<li class="nav-item"><a href="./profile.html" class="nav-link">Profile</a></li>
		<li class="nav-item"><a href="./logout.html" class="nav-link">Logout</a></li>
	`
}