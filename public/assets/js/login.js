// target form component inside login document
let loginForm = document.querySelector('#loginUser');

// login form to be used by the client to insert account to be authenticated by the app
loginForm.addEventListener("submit", (event) => {
	event.preventDefault()

	// capture the values of form components
	let email = document.querySelector('#userEmail').value
	let pass = document.querySelector('#password').value

	// checker
	// console.log(email)
	// console.log(pass)

	// validate the data inside the input fields
	if (email === "" || pass === "") {
		Swal.fire({
			icon: "info",
			text: "Please input email and/or password"
		})
	} else {
		fetch("https://desolate-ocean-63477.herokuapp.com/api/users/login",{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: pass
			})
		}).then(res => {
			return res.json()
		}).then(data => {
			// console.log(data.accessToken)
			// save the generated access token inside the local storage property of the browser
			if (data.accessToken) {
				localStorage.setItem('token', data.accessToken)
				// Swal.fire("Login Successful!")

				// identify access rights for the user
				fetch('https://desolate-ocean-63477.herokuapp.com/api/users/details',{
					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}
				}).then(res => {
					return res.json()
				}).then(data => {
					// console.log(data)
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
				
					// how to redirect (from login.html to profile.html)
					window.location.replace('./profile.html')
				})
			} else {
				// this block of code will run if no access token was generated
				Swal.fire({
					icon: "error",
					text: "Login Failed"
				})
			}
		})
	}
})