// get the value of the access toke  inside the local storage and place it inside a new variable
let token = localStorage.getItem("token")
const isAdmin = localStorage.getItem("isAdmin")
let dispCont = document.querySelector("#profileContainer")


// display information about the user
fetch('https://desolate-ocean-63477.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
}).then(res => res.json()).then(jsonData => {

	console.log(jsonData.enrollments)

/*	let userSubjects = jsonData.enrollments.map(subject => {
		console.log(subject)

		return (`
				<tr>
					<td>${courseDetails}</td>
					<td>${subject.enrolledOn}</td>
					<td>${subject.status}</td>
				</tr>
			`)

	}).join("")
*/
	jsonData.enrollments.map(subject => {
		fetch(`https://desolate-ocean-63477.herokuapp.com/api/courses/${subject.courseId}`).then(res => res.json()).then(data => {
		console.log(data)
		console.log(data.name)
		console.log(data.description)

		document.querySelector('.courseDetails').insertAdjacentHTML('afterbegin',
			`
			<tr>
				<td>${data.name}</td>
				<td>${data.description}</td>
				<td>${subject.enrolledOn}</td>
				<td>${subject.status}</td>
			</tr>
			`
			)

		})

	})

	//section to display user information
		dispCont.innerHTML = `
		<div class="col-md-12">
		<div><a href="./courses.html" class="btn btn-outline-primary btn-block">See Courses Available<a></div>
			<section class="jumbotron my-5">
				<h3 class="text-center">First Name: ${jsonData.firstName}</h3>
				<h3 class="text-center">Last Name: ${jsonData.lastName}</h3>
				<h3 class="text-center">Email: ${jsonData.email}</h3>
				<h3 class="text-center">Mobile Number: ${jsonData.mobileNo}</h3>

				<table class="table">
					<tr>
						<th>Course Name:</th>
						<th>Description:</th>
						<th>Enrolled On:</th>
						<th>Status:</th>
						<tbody class="courseDetails"></tbody>
					</tr>
				</table>

			</section>
		</div>
		`

	
})

if (token===null) {
	window.location.replace('./login.html')
}

if (isAdmin == "true") {
	window.location.replace('./courses.html')
}
