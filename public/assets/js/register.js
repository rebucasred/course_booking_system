let registerForm = document.querySelector("#registerUser")

let errMessage = document.querySelector("#errMessage").value

registerForm.addEventListener("submit", (event) => {
    event.preventDefault()

	let fName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let email = document.querySelector("#userEmail").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let password = document.querySelector("#password1").value
	let verifyPassword = document.querySelector("#password2").value
    
  
   if((fName  !== "" && lastName !== ""  && email !== ""  && password !== ""  && verifyPassword !== "" ) && (password === verifyPassword) && (mobileNo.length === 11))
   {  
      fetch('https://desolate-ocean-63477.herokuapp.com/api/users/email-exists', {
         method: 'POST',
         headers: {
             'Content-Type': 'application/json'
         }, 
         body: JSON.stringify({
            email: email
         })
      }).then(res => {
          return res.json()
      }).then(convertedData => {
          if (convertedData === false) {
          
               fetch('https://desolate-ocean-63477.herokuapp.com/api/users/register', {
          
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  firstName: fName,
                  lastName: lastName,
                  email: email,
                  mobileNo: mobileNo, 
                  password: password
                }) 
              }).then(res => {
                console.log(res)
                return res.json()
              }).then(data => {
                console.log(data)
                if(data === true){
                   Swal.fire({
							icon: "success",
							title: "Success",
							text: "New account registered"
						})
                   window.location.replace('./login.html')
                }else{
                    Swal.fire("Something with wrong during processing!")
                }
              }
              )
           } else {
               Swal.fire({
					icon: "error",
					title: "Error",
					text: "Email already exists"
				})
          }
      })
   }else{
       Swal.fire({
			icon: "warning",
			title: "Check all fields",
			text: "You have left an empty field, please enter value to proceed"
		})
   }
}) 


if(password===verifyPassword){
  errMessage.innerHTML = "Password Match";
}else{
  errMessage.innerHTML = "Password Did Not Match";
}