let urlValues =  new URLSearchParams(window.location.search)

let id = urlValues.get('courseId')
let userId = localStorage.getItem("id");
console.log(id) //checker

let token = localStorage.getItem('token')

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let desc = document.querySelector("#courseDesc")
let enroll = document.querySelector("#enrollmentContainer")

// send a request going to the endpoint that will allow to get and display the course details 
fetch(`https://desolate-ocean-63477.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
	console.log(convertedData)

	name.innerHTML = convertedData.name
	price.innerHTML = convertedData.price
	desc.innerHTML = convertedData.description	
	enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`

	document.querySelector("#enrollButton").addEventListener("click", () => {

		//insert the course inside the enrollments array of the user
		console.log("id: " + id)
		console.log("userId: " + userId)
		fetch('https://desolate-ocean-63477.herokuapp.com/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json', 
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: id,
				userId: userId
			})
		})
		.then(res => {
			return res.json()
		}).then(convertedResponse => {
			if (convertedResponse.message === "Course enrolled successfully") {
				Swal.fire({
					icon: 'success',
					text: `${convertedResponse.message}`
				})	
			} else {
				Swal.fire({
					icon: 'info',
					text: `${convertedResponse.message}`
				})
			}
		})
	})
})

if (token===null) {
	window.location.replace('./login.html')
}