const pass = document.querySelector("#password1")
const pass2 = document.querySelector("#password2")
const err = document.querySelector("#errMessage")
const lower = document.querySelector("#lower")
const upper = document.querySelector("#upper")
const number = document.querySelector("#number")
const special = document.querySelector("#special")
const length = document.querySelector("#length")

pass.addEventListener('focus', () => {
	document.querySelector("#message").style.display = "block"
})

/*pass2.onkeyup = function(){
	if(pass!==pass2){
		err.innerHTML=`<p>Password did not match</p>`
	}else{
		err.innerHTML=`<p>Password match</p>`
	}
}*/

pass.onkeyup = function(){

	/*if(pass!==pass2){
		err.innerHTML=`<p>Password did not match</p>`
	}else{
		err.innerHTML=`<p>Password match</p>`
	}*/
	
	let lowerCase = /[a-z]/g;
	if (pass.value.match(lowerCase)) {
		lower.classList.remove("invalid");
		lower.classList.add("valid");
	}else{
		lower.classList.add("invalid");
		lower.classList.remove("valid");
	}

	let upperCase = /[A-Z]/g;
	if (pass.value.match(upperCase)) {
		upper.classList.remove("invalid");
		upper.classList.add("valid");
	}else{
		upper.classList.add("invalid");
		upper.classList.remove("valid");
	}

	let numCheck = /[0-9]/g;
	if (pass.value.match(numCheck)) {
		number.classList.remove("invalid");
		number.classList.add("valid");
	}else{
		number.classList.add("invalid");
		number.classList.remove("valid");
	}

	if((pass.value.length >=8)&&(pass.value.length <=15)){
		length.classList.remove("invalid");
		length.classList.add("valid");
	}else{
		length.classList.add("invalid");
		length.classList.remove("valid");
	}

	let spChar = /[!@#$%^&*]/g;
	if (pass.value.match(spChar)) {
		special.classList.remove("invalid");
		special.classList.add("valid");
	}else{
		special.classList.add("invalid");
		special.classList.remove("valid");
	}

}