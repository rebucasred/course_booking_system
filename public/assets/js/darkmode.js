const colorSwitch = document.getElementById('inputSwitch');
let darkMode = localStorage.getItem("darkMode");

colorSwitch.addEventListener('click',checkMode);

if(darkMode === 'enabled'){
	darkModeOn();
	colorSwitch.checked = true;
}

function checkMode(){
	if (colorSwitch.checked) {
		darkModeOn();
	} else {
		darkModeOff();
	}
}

function darkModeOn(){
	document.body.classList.add("dark-mode");
	localStorage.setItem("darkMode", "enabled");
}

function darkModeOff(){
	document.body.classList.remove("dark-mode");
	localStorage.setItem("darkMode", null);
}